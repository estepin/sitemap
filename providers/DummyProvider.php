<?php
namespace Sitemap\providers;

use Sitemap\dto\PageDTO;
use Sitemap\interfaces\ProviderInterface;

class DummyProvider implements ProviderInterface
{
    protected $pages;

    function __construct($pages)
    {
        foreach ($pages as $url) {
            $this->pages[] = new PageDTO($url, date('Y-m-d\TH:i:s+00:00'));
        }
    }

    function getPages()
    {
        return $this->pages;
    }
}