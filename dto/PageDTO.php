<?php
namespace Sitemap\dto;

class PageDTO
{
    public $url;
    public $lastModified;

    function __construct($url, $lastModified = false)
    {
        $this->url = $url;
        $this->lastModified = $lastModified;
    }
}