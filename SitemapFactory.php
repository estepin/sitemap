<?php
namespace Sitemap;


use Sitemap\providers\DummyProvider;

class SitemapFactory
{
    static function build()
    {
        $pages = [
            '/', '/catalog', '/services', '/offers', '/news', '/companies', '/map', '/contacts'
        ];

        return new Sitemap(
            new PagesCollector([
                'pages' => new DummyProvider($pages)
            ]),
            new MapGenerator('http://bmb56/'),
            new XmlStorageFile(__DIR__.'/result/')
        );
    }
}