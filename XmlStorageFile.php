<?php
namespace Sitemap;

use Sitemap\interfaces\SitemapStorageInterface;

class XmlStorageFile implements SitemapStorageInterface
{
    protected $path;

    function __construct($path)
    {
        $this->path = $path;
    }

    function save($xmlString)
    {
        $filename = $this->getFilename();
        if(!file_put_contents($filename, $xmlString)) {
            throw new \Exception("Can't save file ".$filename);
        }
    }

    function getFilename()
    {
        return $this->path.'sitemap.xml';
    }
}