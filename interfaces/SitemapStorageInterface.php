<?php
namespace Sitemap\interfaces;

interface SitemapStorageInterface
{
    function save($xmlString);
}