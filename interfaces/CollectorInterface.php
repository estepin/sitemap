<?php
namespace Sitemap\interfaces;

/**
 * Сборщик страниц
 *
 * Собирает страницы для sitemap
 *
 * @package Sitemap
 */

interface CollectorInterface
{
    function collectAll();

    function addProvider($name, ProviderInterface $provider);

    function collectFrom($name);
}