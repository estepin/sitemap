<?php
namespace Sitemap\interfaces;

/**
 * Возвращает pages
 *
 * @package Sitemap\interfaces
 */

interface ProviderInterface
{
    function getPages();
}