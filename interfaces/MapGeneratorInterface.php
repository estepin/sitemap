<?php
namespace Sitemap\interfaces;

/**
 * Генератор sitemap
 *
 * Генерирует sitemap из полученных страниц
 *
 * @package Sitemap
 * @author Evgeniy Stepin
 *
 */

interface MapGeneratorInterface
{
    /**
     * @param $pages array Массив страниц PageDTO
     * @return string Возвращает строку xml
     */

    function generate($pages): string;
}