<?php
namespace Sitemap;

use Sitemap\interfaces\CollectorInterface;
use Sitemap\interfaces\ProviderInterface;

/**
 * Сборщик страниц
 *
 * Собирает страницы для sitemap
 *
 * @package Sitemap
 */
class PagesCollector implements CollectorInterface
{
    protected $providers;

    function __construct($providers = false)
    {
        if($providers) {
            $this->checkProvidersArr($providers);
            $this->providers = $providers;
        }
    }

    /**
     * Добавить провайдера
     *
     * @param $name string Имя провайдера
     * @param ProviderInterface $provider Провайдер
     * @throws \Exception
     */

    function addProvider($name, ProviderInterface $provider)
    {
        if(isset($this->providers[$name])) {
            throw new \Exception('This provider already exists');
        }
        $this->providers[$name] = $provider;
    }

    /**
     * Собрать все страницы со всех провайдеров
     *
     * @return array
     */

    function collectAll()
    {
        $pages = [];
        foreach ($this->providers as $provider) {
            $pages = array_merge($pages, $provider->getPages());
        }

        return $pages;
    }

    /**
     * Собрать все страницы с конкретного провайдера
     *
     * @return array
     */

    function collectFrom($name)
    {
        if(!isset($this->providers[$name])) {
            throw new \Exception('Unknown provider '.$name);
        }
        $provider = $this->providers[$name];
        return $provider->getPages();
    }

    protected function checkProvidersArr($arr)
    {
        foreach ($arr as $k => $provider) {
            if(!($provider instanceof ProviderInterface)) {
                throw new \Exception();
            }
        }
    }
}