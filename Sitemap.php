<?php

namespace Sitemap;

use Sitemap\interfaces\CollectorInterface;
use Sitemap\interfaces\MapGeneratorInterface;
use Sitemap\interfaces\ProviderInterface;
use Sitemap\interfaces\SitemapStorageInterface;

class Sitemap
{
    protected $collector;
    protected $mapGenerator;
    protected $sitemapStorage;

    function __construct(CollectorInterface $collector, MapGeneratorInterface $mapGenerator, SitemapStorageInterface $storage)
    {
        $this->collector = $collector;
        $this->mapGenerator = $mapGenerator;
        $this->sitemapStorage = $storage;
    }

    function runGenerate($returnString = false)
    {
        $pages = $this->collector->collectAll();
        $xmlString = $this->mapGenerator->generate($pages);
        if (!$returnString) {
            $this->sitemapStorage->save($xmlString);
        }
    }

    function addProvider($name, ProviderInterface $provider)
    {
        $this->collector->addProvider($name, $provider);
    }
}