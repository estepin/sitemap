<?php
namespace Sitemap;

use Sitemap\dto\PageDTO;

/**
 * Генератор sitemap
 *
 * Генератор sitemap на вход получает массив $pages содержащий PageDto
 *
 * @package Sitemap
 * @author Evgeniy Stepin
 *
 */

class MapGenerator implements interfaces\MapGeneratorInterface
{
    protected $baseUrl;

    function __construct($baseUrl)
    {
        if(preg_match('|/$|is', $baseUrl) !== false) {
            $baseUrl = substr($baseUrl, 0, strlen($baseUrl) - 1);
        }

        $this->baseUrl = $baseUrl;
    }

    /**
     * Генерирует  sitemap xml из массива
     *
     * @param $pages array Массив c PageDto
     * @return string
     */
    function generate($pages): string
    {
        $xml = new \SimpleXMLElement('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>');
        foreach ($pages as $page) {
            $urlNode = $xml->addChild('url');
            if($this->getUrl($page)) {
                $urlNode->addChild('loc', $this->getUrl($page));
            }

            if($page->lastModified) {
                $urlNode->addChild('last-modified', $page->lastModified);
            }

        }
        return $xml->asXML();
    }

    protected function getUrl(PageDTO $page)
    {
        if(preg_match('|^/|is', $page->url) !== false) {
            $url = $page->url;

        } else {
            $url = '/'.$page->url;
        }

        return $this->baseUrl.$url;
    }
}